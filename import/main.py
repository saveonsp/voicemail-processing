import io
import json
import yaml
import base64
import eml_parser
from pathlib import Path
from pprint import pprint
from datetime import datetime
from tinydb import TinyDB, Query


def process_eml(content: bytes):
    ep = eml_parser.EmlParser(include_attachment_data=True)
    parsed_eml = ep.decode_email_bytes(content)

    h = parsed_eml["header"]["header"]

    sendername, duration, phone, date = [
        h[k][0].strip()
        for k in [
            "x-voicemessagesendername",
            "x-voicemessageduration",
            "x-callingtelephonenumber",
            "date",
        ]
    ]
    assert len(phone) == 10, "invalid phone length!"

    date = datetime.strptime(date, "%a, %d %b %Y %H:%M:%S %z")
    date = int(date.timestamp())

    attachments = parsed_eml["attachment"]
    if (l := len(attachments)) != 1:
        raise Exception(f"unexpected attachment length ({l})")

    attachment = attachments[0]
    attachment_filename, attachment_size, buf = [
        attachment[k] for k in ["filename", "size", "raw"]
    ]

    if not attachment_filename.lower().endswith(".wav"):
        raise Exception(f"unexpected file attachment filetype! ({attachment_filename})")

    buf = base64.b64decode(buf)

    return phone, date, sendername, buf


if __name__ == "__main__":
    with open("config.yaml") as f:
        config = yaml.safe_load(f)

    db = TinyDB(config["db"])

    output_dir = Path(config["dirs"]["output"])
    if not output_dir.is_dir():
        output_dir.mkdir()

    incoming_data_dir = Path(config["dirs"]["input"])

    for fp in incoming_data_dir.glob("*.eml"):
        with open(fp, "rb") as f:
            phone, date, sendername, audio_data = process_eml(f.read())

        audio_file = f"{phone}_{date}.wav"
        with open(output_dir / audio_file, "wb") as f:
            f.write(audio_data)

        db.insert(
            {
                "phone": phone,
                "date": date,
                "sendername": sendername,
                "audio_file": audio_file,
            }
        )
