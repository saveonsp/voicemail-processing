export const environment = {
  production: false,
  useHash: false,
  indexedDBVersion: 1,
};
