import { TestBed } from '@angular/core/testing';

import { EmlService } from './eml.service';

describe('EmlService', () => {
  let service: EmlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
