import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { EmlModule } from './eml/eml.module';

import { AppComponent } from './app.component';
import { AudioVisComponent } from './audio-vis/audio-vis.component';
import { ListPageComponent } from './list-page/list-page.component';
import { ImportPageComponent } from './import-page/import-page.component';
import { AudioRawVisComponent } from './audio-raw-vis/audio-raw-vis.component';
import { ListVoicemailsPageComponent } from './list-voicemails-page/list-voicemails-page.component';
import { AudioVisLocalComponent } from './audio-vis-local/audio-vis-local.component';
import { FileDropComponent } from './file-drop/file-drop.component';
import { WaveformComponent } from './waveform/waveform.component';

@NgModule({
  declarations: [
    AppComponent,
    AudioVisComponent,
    ListPageComponent,
    ImportPageComponent,
    AudioRawVisComponent,
    ListVoicemailsPageComponent,
    AudioVisLocalComponent,
    FileDropComponent,
    WaveformComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    EmlModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
