import { ElementRef, ViewChild, Component, OnInit } from '@angular/core';
import { of, ReplaySubject, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { EmlService } from '../eml.service';
import { EMLParseResult } from '../models';

@Component({
  selector: 'app-import-page',
  template: `
    <header>
      <div class="container">
        <h1>Import EML File</h1>
      </div>
    </header>
    <div class="container">
      <ng-container *ngIf="files$ | async as files; else alt">
        <ng-container *ngIf="progress$ | async as progress">
          <section>
            <span>{{ progress.loading ? 'Loading...' : 'Complete!' }}</span>
            <mat-progress-bar [value]="progress.percent"></mat-progress-bar>
          </section>
          <section>
            <div class="files">
              <div
                *ngFor="let each of progressResults$ | async"
                [title]="each.filename"
              >
                <span>{{ each.filename }}</span>
                <mat-icon>{{
                  each.loading ? '' : each.error ? 'remove' : 'done'
                }}</mat-icon>
              </div>
            </div>
          </section>
          <section>
            <div *ngIf="!progress.loading" class="button-list">
              <a [routerLink]="['/list']" mat-flat-button color="primary"
                >Begin Processing</a
              >
              <button mat-flat-button (click)="files$.next(null)">
                Import More
              </button>
            </div>
          </section>
        </ng-container>
      </ng-container>
      <ng-template #alt>
        <app-file-drop
          [accept]="'.eml'"
          (files)="files$.next($event)"
        ></app-file-drop>
      </ng-template>
    </div>
  `,
  styles: [
    `
      header h1 {
        margin: 20px 8px 8px 8px;
      }
    `,
    `
      :host {
        display: block;
      }
      section {
        margin-bottom: 20px;
      }
      :host > .container {
        padding: 0 8px;
      }
      input[type='file'] {
        display: none;
      }
      .drag-area {
        padding: 3em 2em 3em 2em;
        border: 1px solid;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        display: flex;
        flex-grow: 1;
      }
      .button-list {
        display: flex;
      }
      .button-list > * {
        margin-right: 8px;
      }
    `,
    `
      .files {
        display: grid;
        grid-auto-rows: auto;
        grid-gap: 12px;
        grid-template-columns: repeat(auto-fill, minmax(140px, 1fr));
      }
      .files > * {
        display: flex;
        align-items: center;
      }
      .files > * span {
        line-height: 40px;
        height: 40px;
        padding: 0 8px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
      }
    `,
  ],
})
export class ImportPageComponent implements OnInit {
  @ViewChild('file')
  input?: ElementRef;

  files$ = new ReplaySubject<File[] | null>(1);

  progressResults$ = this.files$.pipe(
    switchMap((files) =>
      files != null ? this.eml.importFiles(files) : of(null)
    )
  );

  progress$ = this.progressResults$.pipe(
    map((results) => {
      if (results) {
        const l = results.length;
        const n = results.filter((v) => !v.loading).length;
        const percent = (n / l) * 100;
        const loading = n !== l;
        return { percent, loading };
      }
      return null;
    })
  );

  voicemails: Observable<EMLParseResult>[] = [];

  constructor(public eml: EmlService) {}

  ngOnInit(): void {}

  onChange(event: any): void {
    this.files$.next(Array.from(this.input?.nativeElement.files) as File[]);
  }

  dropHandler(e: Event): void {
    e.preventDefault();
    console.log(e);
  }

  dragOverHandler(e: Event): void {
    e.preventDefault();
    console.log(e);
  }
}
