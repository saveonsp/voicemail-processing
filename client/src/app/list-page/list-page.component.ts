import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-list-page',
  template: `
    <!--
    <ng-container *ngIf="files$ | async as files">
      <app-audio-vis
        *ngFor="let file of files"
        [filename]="file"
      ></app-audio-vis>
    </ng-container>
    -->
  `,
  styles: [
    `
      app-audio-vis {
        margin-bottom: 20px;
      }
    `,
  ],
})
export class ListPageComponent implements OnInit {
  files$ = this.http.get<string[]>(`/data/`);

  constructor(public http: HttpClient) {}

  ngOnInit(): void {}
}
