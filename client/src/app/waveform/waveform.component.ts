import {
  AfterViewInit,
  Input,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Component,
  OnInit,
} from '@angular/core';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-waveform',
  template: ` <audio controls [src]="url"></audio> `,
  styles: [],
})
export class WaveformComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input()
  buffer?: ArrayBuffer;

  url?: SafeUrl;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}
}
