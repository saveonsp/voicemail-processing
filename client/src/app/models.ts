export interface EMLParseResult {
  index: number;
  id: string;
  phone: string;
  duration: number;
  sendername: string;
  date: number;
  filename: string;
  filesize: number;
  audio: ArrayBuffer;
  processed: number; // boolean
  exported: number; // boolean
}

export interface WorkInput {
  index: number;
  file: File;
}
