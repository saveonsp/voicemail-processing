import {
  ModuleWithProviders,
  SkipSelf,
  Optional,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmlService } from '../eml.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
})
export class EmlModule {
  constructor(@Optional() @SkipSelf() parentModule?: EmlModule) {
    if (parentModule) {
      throw new Error('EmlModule is already loaded.');
    }
  }

  static forRoot(): ModuleWithProviders<EmlModule> {
    return {
      ngModule: EmlModule,
      providers: [
        EmlService,
        // {provide: UserServiceConfig, useValue: config }
      ],
    };
  }
}
