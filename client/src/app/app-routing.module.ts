import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from '../environments/environment';
import { ListPageComponent } from './list-page/list-page.component';
import { ImportPageComponent } from './import-page/import-page.component';
import { ListVoicemailsPageComponent } from './list-voicemails-page/list-voicemails-page.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/list' },
  { path: 'import', component: ImportPageComponent },
  { path: 'list', component: ListVoicemailsPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: environment.useHash })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
