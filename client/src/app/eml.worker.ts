/// <reference lib="webworker" />
import { DoWorkUnit, ObservableWorker } from 'observable-webworker';
import { EMPTY, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import PostalMime from 'postal-mime';

import { environment } from '../environments/environment';
import { setupIndexedDB } from './util';
import { EMLParseResult, WorkInput } from './models';

const HEADER_KEYS = [
  'x-callingtelephonenumber',
  'x-voicemessageduration',
  'x-voicemessagesendername',
];

interface PostalMimeResult {
  attachments: {
    filename: string;
    mimeType: string;
    disposition: string;
    related: boolean;
    contentId: string;
    content: ArrayBuffer;
  }[];
  date: string;
  headers: {
    key: string;
    value: string;
  }[];
}

@ObservableWorker()
export class WorkerPoolHashWorker
  implements DoWorkUnit<WorkInput, EMLParseResult> {
  db?: IDBDatabase;

  public workUnit({ file, index }: WorkInput): Observable<EMLParseResult> {
    const { name: filename, size: filesize } = file;
    return this.readFileAsArrayBuffer(file).pipe(
      switchMap(async (arrayBuffer) => {
        const record = (await this.parseEMLFile(arrayBuffer)) as EMLParseResult;
        record.filename = filename;
        record.filesize = filesize;
        record.processed = 0;
        record.exported = 0;
        await this.saveRecord(record);
        return { ...record, index };
      })
    );
  }

  private async parseEMLFile(
    buf: ArrayBuffer
  ): Promise<Partial<EMLParseResult>> {
    const result = (await new PostalMime().parse(buf)) as PostalMimeResult;

    const date = Date.parse(result.date);
    const attachments = result.attachments;

    const audio = attachments.length > 0 ? attachments[0].content : undefined;

    const o = result.headers
      .filter(({ key }) => HEADER_KEYS.includes(key))
      .reduce(
        (acc, { key, value }) => ({ ...acc, [key]: value }),
        {} as { [k: string]: string }
      );

    const values = HEADER_KEYS.map((k) => o[k]);
    const phone = values[0];
    const duration = parseInt(values[1], 10);
    const sendername = values[2];

    const id = [phone, date].join('_');
    return {
      id,
      phone,
      duration,
      sendername,
      date,
      audio,
    };
  }

  private saveRecord(record: EMLParseResult): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!this.db) {
        throw new Error('db not initialized!!!');
      }
      const r = this.db
        .transaction(['voicemails'], 'readwrite')
        .objectStore('voicemails')
        .put(record);
      r.onsuccess = (e) => resolve();
      r.onerror = (e) => reject(r.error);
    });
  }

  private readFileAsArrayBuffer(blob: Blob): Observable<ArrayBuffer> {
    return new Observable((observer) => {
      if (!(blob instanceof Blob)) {
        observer.error(
          new Error('`blob` must be an instance of File or Blob.')
        );
        return;
      }

      const reader = new FileReader();

      reader.onerror = (err) => observer.error(err);
      reader.onload = () => observer.next(reader.result as ArrayBuffer);
      reader.onloadend = () => observer.complete();

      reader.readAsArrayBuffer(blob);

      return () => reader.abort();
    });
  }

  constructor() {
    const r = indexedDB.open('voicemails', environment.indexedDBVersion);
    let db: IDBDatabase;
    r.onsuccess = (e) => {
      db = r.result;
      this.db = db;
    };
    r.onupgradeneeded = (e) => {
      db = r.result;
      setupIndexedDB(db);
      this.db = db;
    };
    r.onerror = (e) => {};
  }
}
