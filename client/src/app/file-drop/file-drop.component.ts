import {
  ViewChild,
  ElementRef,
  HostListener,
  HostBinding,
  Input,
  Output,
  EventEmitter,
  Component,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'app-file-drop',
  template: `
    <div class="drag-area" (drop)="drop($event)" (dragover)="dragover($event)">
      <input
        type="file"
        multiple
        [accept]="accept"
        #file
        (change)="change($event)"
      />
      <button mat-flat-button color="primary" (click)="file.click()">
        Upload file
      </button>
      <p>Or drag files here.</p>
    </div>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        border: 1px solid grey;
        margin: 8px;
        padding: 2em;
      }
      :host.draggedover {
        background-color: rgba(0, 0, 0, 0.2);
      }
      input[type='file'] {
        display: none;
      }
    `,
  ],
})
export class FileDropComponent implements OnInit {
  @ViewChild('file')
  input?: ElementRef;

  @Input()
  accept?: string;

  @Output()
  files = new EventEmitter<File[]>();

  @HostBinding('class.draggedover')
  draggedover = false;

  constructor() {}

  ngOnInit(): void {}

  @HostListener('drop', ['$event'])
  drop(evt: DragEvent): void {
    this.draggedover = false;
    evt.preventDefault();
    const files = evt.dataTransfer?.files;
    if (files?.length) {
      this.emit(files);
    }
  }

  @HostListener('dragover', ['$event'])
  dragover(evt: DragEvent): void {
    this.draggedover = true;
    evt.preventDefault();
  }

  @HostListener('dragleave', ['$event'])
  dragleave(evt: DragEvent): void {
    this.draggedover = false;
    evt.preventDefault();
  }

  change(evt: Event): void {
    const fileList = this.input?.nativeElement.files as FileList;
    this.emit(fileList);
  }

  emit(fileList: FileList): void {
    this.files.next(Array.from(fileList));
  }
}
