import {
  AfterViewInit,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  Component,
  OnInit,
} from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { environment } from '../../environments/environment';

import * as Peaks from 'peaks.js';

@Component({
  selector: 'app-audio-vis',
  template: `
    <audio #audio [src]="url" controls autoplay="false"></audio>
    <div #zoomview></div>
    <div #overview></div>
  `,
  styles: [
    `
      :host {
        display: block;
      }
      audio {
        width: 100%;
      }
    `,
  ],
})
export class AudioVisComponent implements AfterViewInit, OnInit, OnChanges {
  @Input()
  url?: SafeUrl | string;

  @ViewChild('audio')
  audio?: ElementRef;

  @ViewChild('zoomview')
  zoomview?: ElementRef;

  @ViewChild('overview')
  overview?: ElementRef;

  peaks?: Peaks.PeaksInstance;
  audioContext = new AudioContext();

  constructor() {}

  ngOnInit(): void {}

  async ngAfterViewInit(): Promise<void> {
    this.updatePeaks();
  }

  // TODO: fix this
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.url && this.peaks != null) {
      this.updatePeaks();
    }
  }

  private async updatePeaks(): Promise<void> {
    const { url, zoomview, overview, audio, audioContext } = this;
    if (zoomview == null || overview == null || audio == null) {
      throw new Error('missing required container');
    }
    if (!url) {
      throw new Error('url required');
    }
    this.peaks = await new Promise((resolve, reject) => {
      if (this.peaks == null) {
        Peaks.init(
          {
            mediaElement: audio.nativeElement,
            webAudio: {
              audioContext,
            },
            containers: {
              zoomview: zoomview.nativeElement,
              // overview: overview.nativeElement,
            },
            height: 200,
          },
          (err, peaks) => (err || !peaks ? reject(err) : resolve(peaks))
        );
        // this.peaks.on('error', (args: any) => console.error(args));
        // this.peaks.setSource(this.options, (err: any) => console.log(err));
      } else {
        this.peaks.setSource(
          {
            mediaUrl: this.url as string,
            webAudio: {
              audioContext,
            },
          },
          (err) => (err ? reject(err) : resolve(this.peaks))
        );
      }
    });
    audio.nativeElement.pause();
    this.peaks?.player.pause();
    this.peaks?.views.getView('zoomview')?.setAmplitudeScale(0.6);
  }
}
