import { Injectable } from '@angular/core';
import { fromWorkerPool } from 'observable-webworker';
import { environment } from '../environments/environment';
import { of, BehaviorSubject, ReplaySubject, Observable, Observer } from 'rxjs';
import {
  ignoreElements,
  scan,
  tap,
  concat,
  switchMap,
  refCount,
  multicast,
} from 'rxjs/operators';

import { EMLParseResult, WorkInput } from './models';
import { setupIndexedDB } from './util';

interface EMLImportResult {
  filename: string;
  error: any;
  result: EMLParseResult | null;
  loading: boolean;
}

@Injectable()
export class EmlService {
  db: Observable<IDBDatabase> = Observable.create(
    (observer: Observer<IDBDatabase>) => {
      const request = indexedDB.open(
        'voicemails',
        environment.indexedDBVersion
      );

      request.onerror = (e) => {
        observer.error((e.target as IDBRequest).error);
      };
      request.onsuccess = (e) => {
        const db = (e.target as IDBRequest).result as IDBDatabase;
        observer.next(db);
        observer.complete();
      };
      (request as IDBOpenDBRequest).onupgradeneeded = (e) => {
        const db = (e.target as IDBRequest).result as IDBDatabase;
        setupIndexedDB(db);
        // observer.next(db);
        // observer.complete();
      };
    }
  ).pipe(multicast(new ReplaySubject()), refCount());

  voicemails$ = this.db.pipe(
    switchMap((db) =>
      Observable.create((observer: any) => {
        const request = db
          .transaction(['voicemails'], 'readonly')
          .objectStore('voicemails')
          .getAll();
        request.onsuccess = (e) => observer.next(request.result);
        request.onerror = (e) => observer.error(request.error);
      })
    )
  );

  importFiles(
    files: File[]
  ): Observable<
    {
      loading: boolean;
      result: EMLParseResult | null;
      error: any;
      filename: string;
    }[]
  > {
    const factory = () => new Worker('./eml.worker', { type: 'module' });

    const red = files.map((file, index) => ({
      filename: file.name,
      result: null,
      error: null,
      loading: true,
    })) as EMLImportResult[];

    return fromWorkerPool<WorkInput, EMLParseResult>(
      factory,
      files.map((file, index) => ({ file, index }))
    ).pipe(
      scan((acc, v) => {
        const o = acc[v.index];
        o.result = v;
        o.loading = false;
        return acc;
      }, red)
    );
  }

  createAudioContex(buf: ArrayBuffer): Observable<AudioContext> {
    return Observable.create((observer: Observer<AudioContext>) => {
      const context = new AudioContext();
      context.decodeAudioData(buf, (buffer) => {
        const source = context.createBufferSource();
        source.buffer = buffer;
        source.connect(context.destination);
        observer.next(context);
        observer.complete();
      });
    });
  }

  constructor() {}
}
