import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioVisLocalComponent } from './audio-vis-local.component';

describe('AudioVisLocalComponent', () => {
  let component: AudioVisLocalComponent;
  let fixture: ComponentFixture<AudioVisLocalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AudioVisLocalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioVisLocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
