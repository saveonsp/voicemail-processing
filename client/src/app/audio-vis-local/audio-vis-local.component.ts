import {
  SimpleChanges,
  Input,
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validator } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

import { EMLParseResult } from '../models';

function formatPhone(s: string): string {
  if (s.length === 10 && Array.from(s).every((c) => !isNaN(parseInt(c, 10)))) {
    return [s.substr(0, 3), s.substr(3, 3), s.substr(6)].join('-');
  }
  return s;
}

@Component({
  selector: 'app-audio-vis-local',
  template: `
    <header>
      <span>{{ voicemailDate | date: 'medium' }}</span>
      <span class="description">{{ description }}</span>
      <button mat-flat-button color="primary" (click)="minimized = !minimized">
        {{ minimized ? 'Edit' : 'Mark Complete' }}
      </button>
    </header>
    <ng-container *ngIf="!minimized">
      <app-audio-vis [url]="url"></app-audio-vis>
      <!--<audio controls [src]="url"></audio>-->
      <form [formGroup]="form">
        <div class="controls">
          <mat-form-field appearance="fill">
            <mat-label>Name</mat-label>
            <input matInput placeholder="John Doe" formControlName="name" />
          </mat-form-field>
          <mat-form-field appearance="fill">
            <mat-label>CHID</mat-label>
            <input matInput formControlName="chid" />
          </mat-form-field>
          <mat-form-field appearance="fill">
            <mat-label>DOB</mat-label>
            <input matInput placeholder="MM/DD/YYYY" formControlName="dob" />
          </mat-form-field>
          <mat-form-field appearance="fill">
            <mat-label>Phone</mat-label>
            <input
              matInput
              placeholder="555-555-5555"
              formControlName="phone"
            />
          </mat-form-field>
          <mat-form-field appearance="fill" class="comments">
            <mat-label>Comments</mat-label>
            <textarea matInput formControlName="comments"></textarea>
          </mat-form-field>
        </div>
        <mat-checkbox formControlName="includeInReport"
          >Include In Report</mat-checkbox
        >
        <mat-checkbox formControlName="includeInReport"
          >Needs follow-up</mat-checkbox
        >
      </form>
      <div></div>
    </ng-container>
  `,
  styleUrls: ['./audio-vis-local.component.scss'],
})
export class AudioVisLocalComponent implements OnInit, OnChanges, OnDestroy {
  @Input() minimized = false;
  @Input()
  voicemail?: EMLParseResult;

  get voicemailDate(): Date | null {
    const s = this.voicemail?.date;
    return s ? new Date(s) : null;
  }

  get description(): string {
    return [this.name.value, formatPhone(this.phone.value), this.comments.value]
      .filter((n) => n)
      .join(', ');
  }

  url?: SafeUrl;

  form = this.fb.group({
    name: [''],
    chid: [''],
    dob: [''],
    phone: [''],
    comments: [''],
    includeInReport: [true],
  });

  get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  get phone(): FormControl {
    return this.form.get('phone') as FormControl;
  }

  get comments(): FormControl {
    return this.form.get('comments') as FormControl;
  }

  constructor(private sanitizer: DomSanitizer, public fb: FormBuilder) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.voicemail) {
      if (this.url) {
        this.deinit(this.url);
      }
      if (changes.voicemail.currentValue) {
        this.url = this.init(changes.voicemail.currentValue.audio);
      }
    }
    if (changes.voicemail) {
      const cv = changes.voicemail.currentValue;
      const { sendername: name, phone } = cv;
      this.form.patchValue({ name, phone });
    }
  }

  ngOnDestroy(): void {
    if (this.url) {
      this.deinit(this.url);
    }
  }

  deinit(url: SafeUrl): void {
    URL.revokeObjectURL(url as string);
  }

  init(buf: ArrayBuffer): SafeUrl {
    const blob = new Blob([buf]);
    const url = URL.createObjectURL(blob);
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
