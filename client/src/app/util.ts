export function setupIndexedDB(db: IDBDatabase): void {
  const store = db.createObjectStore('voicemails', { keyPath: 'id' });
  store.createIndex('phone', 'phone', { unique: false });
  store.createIndex('date', 'date', { unique: false });
  store.createIndex('processed', 'processed', { unique: false });
  store.createIndex('exported', 'exported', { unique: false });
}
