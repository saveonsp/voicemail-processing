import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioRawVisComponent } from './audio-raw-vis.component';

describe('AudioRawVisComponent', () => {
  let component: AudioRawVisComponent;
  let fixture: ComponentFixture<AudioRawVisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AudioRawVisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioRawVisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
