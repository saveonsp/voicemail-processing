import {
  Input,
  AfterViewInit,
  ElementRef,
  SimpleChanges,
  ViewChild,
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
} from '@angular/core';
import { Observer, Observable, Subject, ReplaySubject } from 'rxjs';
import {
  multicast,
  refCount,
  withLatestFrom,
  tap,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import * as Peaks from 'peaks.js';
import { EmlService } from '../eml.service';

@Component({
  selector: 'app-audio-raw-vis',
  template: `
    <button mat-icon-button (click)="peaks?.player?.play()">
      <mat-icon>play_arrow</mat-icon>
    </button>
    <button mat-icon-button (click)="peaks?.player?.pause()">
      <mat-icon>pause</mat-icon>
    </button>
    <audio #audio controls autoplay="false"></audio>
    <div #zoomview></div>
  `,
  styles: [],
})
export class AudioRawVisComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @ViewChild('audio')
  audio?: ElementRef;

  @ViewChild('zoomview')
  zoomview?: ElementRef;

  @Input()
  id?: string;

  id$ = new ReplaySubject<string>(1);
  destroyed$ = new Subject();

  audioContext = new AudioContext();

  peaks?: Peaks.PeaksInstance;

  constructor(public eml: EmlService) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.id$
      .pipe(
        withLatestFrom(this.eml.db),
        switchMap(([id, db]) =>
          Observable.create((observer: Observer<any>) => {
            db
              .transaction(['voicemails'], 'readonly')
              .objectStore('voicemails')
              .get(id).onsuccess = (e) => {
              observer.next((e.target as IDBRequest).result);
              observer.complete();
            };
          })
        ),
        switchMap((v: any) => this.audioContext.decodeAudioData(v.audio)),
        switchMap((audioBuffer) =>
          Observable.create((observer: Observer<Peaks.PeaksInstance>) => {
            // const source = this.audioContext.createBufferSource();
            // source.connect(this.audioContext.destination);
            // source.buffer = audioBuffer;
            // source.start(0);

            const options = {
              containers: {
                zoomview: this.zoomview?.nativeElement,
              },
              mediaElement: this.audio?.nativeElement,
              webAudio: {
                audioContext: this.audioContext,
                audioBuffer,
              },
            };
            Peaks.init(options, (err, peaks) => {
              if (err) {
                observer.error(err);
              } else {
                observer.next(peaks as Peaks.PeaksInstance);
              }
            });
          })
        ),
        takeUntil(this.destroyed$)
      )
      .subscribe((peaks) => (this.peaks = peaks as Peaks.PeaksInstance));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('id' in changes) {
      this.id$.next(changes.id.currentValue);
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
