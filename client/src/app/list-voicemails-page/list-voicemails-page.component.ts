import { Component, OnInit } from '@angular/core';
import { EmlService } from '../eml.service';
import { BehaviorSubject, Observer, Observable } from 'rxjs';
import { shareReplay, withLatestFrom, switchMap } from 'rxjs/operators';

const DELIMITER = ',';
const STORE_NAME = 'voicemails';
const IDENTITY = (v: any) => v;

@Component({
  selector: 'app-list-voicemails-page',
  template: `
    <header>
      <div class="container">
        <h1>Voicemails</h1>
        <span *ngIf="count$ | async as count">Total: {{ count.total }}</span>
      </div>
    </header>
    <section *ngIf="voicemails$ | async as voicemails">
      <div class="container">
        <ng-container *ngIf="voicemails?.length; else alt">
          <app-audio-vis-local
            *ngFor="let voicemail of voicemails"
            [voicemail]="voicemail"
          ></app-audio-vis-local>
        </ng-container>
        <ng-template #alt>No voicemails loaded.</ng-template>
      </div>
    </section>
    <section>
      <div class="container">
        <a mat-flat-button color="primary" [routerLink]="['/import']"
          >Import More</a
        >
        <button mat-flat-button (click)="generateExport()">Export</button>
      </div>
    </section>
  `,
  styles: [
    `
      :host {
        display: block;
      }
      :host > section > .container {
        padding: 0 8px;
      }
      :host > section {
        margin-bottom: 40px;
      }
      header > .container {
        display: flex;
        align-items: baseline;
        justify-content: space-between;
        padding: 20px 8px 12px 8px;
      }
      .container > * {
        margin-bottom: 40px;
      }
    `,
  ],
})
export class ListVoicemailsPageComponent implements OnInit {
  pos$ = new BehaviorSubject({ offset: 0, limit: 10 });

  voicemails$: Observable<any[]> = this.eml.db.pipe(
    withLatestFrom(this.pos$),
    switchMap(
      ([db, { limit, offset }]) =>
        Observable.create((observer: Observer<any[]>) => {
          const store = db
            .transaction([STORE_NAME], 'readonly')
            .objectStore(STORE_NAME);

          // store.index('processed').count(IDBKeyRange.only(1))

          let i = 0;
          const values = [] as any[];
          store.index('date').openCursor().onsuccess = (e: any) => {
            const cursor = e.target.result as IDBCursorWithValue;
            if (cursor && i < limit) {
              values.push(cursor.value);
              cursor.continue();
              i++;
            } else {
              observer.next(values);
              observer.complete();
              // done
            }
          };
        }) as Observable<any[]>
    )
  );

  count$: Observable<{ total: number }> = this.eml.db.pipe(
    switchMap(
      (db) =>
        Observable.create((observer: Observer<{ total: number }>) => {
          const store = db
            .transaction([STORE_NAME], 'readonly')
            .objectStore(STORE_NAME);
          const counts = { total: 0, processed: 0, unprocessed: 0 };
          store.openCursor().onsuccess = (e: any) => {
            const cursor = e.target.result as IDBCursorWithValue;
            if (cursor) {
              counts.total += 1;
              cursor.continue();
            } else {
              observer.next(counts);
              observer.complete();
            }
          };
        }) as Observable<{ total: number }>
    ),
    shareReplay(1)
  );

  generateExport(): void {
    this.export().subscribe((records) => {
      const filename = 'export.csv';
      const text = records
        .map((row) =>
          row
            .map(
              (cell) => (
                (cell = `${cell}`),
                cell.includes(DELIMITER) ? `"${cell}"` : cell
              )
            )
            .join(DELIMITER)
        )
        .join('\n');
      const blob = new Blob([text], { type: 'text/plain' });
      const url = URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = filename;
      a.click();
      URL.revokeObjectURL(url);
    });
  }

  export(): Observable<any[][]> {
    return this.eml.db.pipe(
      switchMap(
        (db) =>
          Observable.create((observer: Observer<any>) => {
            const fns = [
              (num: number) => new Date(num).toISOString(),
              null,
              null,
              null,
            ];
            const columns = ['date', 'phone', 'sendername', 'comment'];
            const store = db
              .transaction([STORE_NAME], 'readonly')
              .objectStore(STORE_NAME);
            const results: any[][] = [];
            store
              .index('exported')
              .openCursor(IDBKeyRange.only(0)).onsuccess = (e: any) => {
              const cursor = e.target.result as IDBCursorWithValue;
              if (cursor) {
                const row = columns.map((k, i) =>
                  (fns[i] || IDENTITY)(cursor.value[k])
                );
                results.push(row);
                cursor.continue();
              } else {
                observer.next(results);
                observer.complete();
              }
            };
          }) as Observable<any>
      )
    );
  }

  constructor(public eml: EmlService) {}

  ngOnInit(): void {}
}
