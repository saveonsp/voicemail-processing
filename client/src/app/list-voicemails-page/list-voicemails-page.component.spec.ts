import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVoicemailsPageComponent } from './list-voicemails-page.component';

describe('ListVoicemailsPageComponent', () => {
  let component: ListVoicemailsPageComponent;
  let fixture: ComponentFixture<ListVoicemailsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListVoicemailsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVoicemailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
