import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <header>
      <nav>
        <div class="container">
          <a [routerLink]="['/']">Voicemail Attachment Processing Tool</a>
          <ul>
            <li>
              <a routerLinkActive="active" [routerLink]="['list']">List</a>
            </li>
            <li>
              <a routerLinkActive="active" [routerLink]="['import']">Import</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <router-outlet></router-outlet>
  `,
  styles: [
    `
      nav > .container {
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
      }
      nav > .container span,
      nav > .container a {
        display: inline-block;
        padding: 0 8px;
        height: 40px;
        line-height: 40px;
      }
      nav > .container a {
        text-decoration: none;
        color: inherit;
      }
      nav > .container a:hover,
      nav > .container a.active {
        text-decoration: underline;
      }
      nav > .container ul {
        list-style-type: none;
        padding: 0;
        margin: 0;
        display: flex;
      }
    `,
  ],
})
export class AppComponent {}
