# Audio processing application

Serve audio files with nginx.  HTML5 audio & waveform visual.  Audio waveform
data is generated server-side then cached and served by nginx.

# Getting Started

1. Move audio files to `audio/original`  
2. Process email messages with `python3 import/main.py`  
3. Start services  
