# Audio processing server

Serve directory contents and waveform data generated with [audiowaveform](https://github.com/bbc/audiowaveform)
on demand (streamed from disk when available).
