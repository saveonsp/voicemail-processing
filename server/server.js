const path = require("path");
const assert = require("assert").strict;
const fs = require("fs");
const { spawn } = require("child_process");
const Koa = require("koa");
const Router = require("@koa/router");
const fg = require("fast-glob");
const config = require("config");

const EXECUTABLE = config.get("audiowaveform_binary");
const EXTENSIONS = config.get("media_extensions");
const MEDIA_DIR = config.get("media_dir");
const DATA_DIR = config.get("data_dir");
const PORT = config.get("port");

const app = new Koa();
const router = new Router();
const running = {};

// list available audio files
router.get("/data/", async (ctx, next) => {
  const files = await fg(
    EXTENSIONS.map((ext) => `**/*${ext}`),
    { cwd: MEDIA_DIR }
  );
  ctx.response.body = JSON.stringify(files);
});

// generate and serve audio waveform data files
router.get("/data/:relpath(.*)", async (ctx, next) => {
  let { relpath } = ctx.params;
  // spooky scary skeletons
  relpath = path.normalize(relpath).replace(/^(\.\.(\/|\\|$))+/, "");

  let basename, suffix, sourcefile;
  try {
    basename = path.basename(relpath);
    for (const s of [".dat", ".json", ".png"]) {
      if (basename.endsWith(s)) {
        suffix = s;
      }
    }
    assert.ok(suffix);

    sourcefile = path.join(
      MEDIA_DIR,
      relpath.slice(0, relpath.length - suffix.length)
    );
    const stats = await fs.promises.stat(sourcefile);
    assert.ok(stats.isFile());
  } catch (e) {
    ctx.throw(404);
  }

  const datafile = path.join(DATA_DIR, relpath);

  let exitCode = 0;
  try {
    // cached version exists
    const stats = await fs.promises.stat(datafile);
    assert.ok(stats.isFile());
  } catch (e) {
    // create one
    if (!(datafile in running)) {
      const args = ["-i", sourcefile, "-o", datafile, "-z", "256", "-b", "8"];
      [exitCode, stdout, stderr] = await new Promise(
        ((key) => (r) => {
          const p = spawn(EXECUTABLE, args);
          let stderr = "",
            stdout = "";
          running[key] = p;
          p.on("stderr", (d) => (stderr += d));
          p.on("stdout", (d) => (stdout += d));
          p.on("close", (code) => {
            delete running[key];
            r([code, stdout, stderr]);
          });
        })(datafile)
      );
      if (exitCode != 0) {
        console.log("error", stderr);
        ctx.throw(500);
      }
    } else {
      exitCode = await new Promise((r) => running[key].on("close", r));
    }
  }

  if (exitCode != 0) {
    ctx.throw(500);
  }

  ctx.response.body = fs.createReadStream(datafile);
});

app.use(router.routes()).use(router.allowedMethods()).listen(PORT);
